<?php

  ob_start();
  session_start();
  include_once 'dbconnect.php';

  if ($connection->connect_errno) {
      echo "Failed to connect to MySQL: " . $mysqli->connect_error;
  }

?>

<!DOCTYPE html>
<html>
<head>
	<title>Marks Entry</title>
</head>
<body>

	<h1>Students Marks Entry Form</h1>

	<form action="/action_page.php">
	  <fieldset>
	    <legend>Personal information:</legend>
	    First name:<br>
	    <input type="text" name="firstname" value="Mickey">
	    <br>
	    Last name:<br>
	    <input type="text" name="lastname" value="Mouse">
	    <br><br>
	    <input type="submit" value="Submit">
	  </fieldset>
	</form>

	<style type="text/css">
		body {
			background:url(images/background.jpg);
			border:0;
			font:14px Tahoma, Geneva, sans-serif;
			color:#4c4c4c;
			line-height:20px;
			padding-bottom:20px;
			min-width:980px
		}

		h1{
			color: #00ff28;
			text-align: center;
			font-size: 50px; 
		}
	</style>

</body>
</html>

