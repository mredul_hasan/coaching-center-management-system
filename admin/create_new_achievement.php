<?php
session_start();

error_reporting(0);
include_once 'dbconnect.php';

//set validation error flag as false
$error = false;

//check if form is submitted
if (isset($_POST['signup'])) {
	$title = mysqli_real_escape_string($connection, $_POST['title']);
	$description = mysqli_real_escape_string($connection, $_POST['description']);
		
	if(isset($_POST['year']) )
	{
	  $year = $_POST['year'];
	  if($year == "void"){
	  	$error = true;
		$year_error = "Please select year!";
	  }
	}
	
	//file upload
$target_dir = "images/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["signup"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 5000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    	
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";

    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

	if (!$error) {
		if(mysqli_query($connection, "INSERT INTO achievement( `year`, `description`, `photo_path`, `title`) VALUES('" . $year . "', '" . $description . "','admin/" . $target_file . "','" . $title . "')")) {
			
			$successmsg = "Successfully Created!";
		} else {
			$errormsg = "Error in creation...Please try again later!";
		}
	}
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Achievement creation Form</title>	
	<meta http-equiv="Content-Type" content="width=device-width, initial-scale=1.0" name="viewport" charset="UTF-8" />
	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
</head>
	<body>
		<header class="site-header">
			<div class="container">
				<a href="/wpl_1/about.php" id="branding">
					<img src="create_achievement.png" alt="" class="logo">
					<div class="logo-copy">
						<h1 class="site-title">Add New Achievement</h1>						
					</div>
				</a> <!-- #branding -->				
			</div>
		</header>

<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4 well">
			<form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data" name="signupform">
				<fieldset>
					<legend>Add New Achievement</legend>

					<div class="form-group">
						<label for="name">Title</label>
						<input type="text" name="title" placeholder="Enter title of achievement" class="form-control" />						
					</div>

					<div class="form-group">
						<label for="name">Description</label>
						<input type="text" name="description" height="48" placeholder="Enter Description of the achievement" class="form-control" />					
					</div>				

					<div class="form-group">
						<label for="name">Year</label>
						<select id="year" name="year">
						  <option value="void" selected="selected">Select a year</option>
						  <option value="2010" >2010</option>
						  <option value="2012" >2012</option>
						  <option value="2013" >2013</option>
						  <option value="2014" >2014</option>
						  <option value="2015" >2015</option>
						  <option value="2016" >2016</option>
						  <option value="2017" >2017</option>
						  <option value="2018" >2018</option>
						  <option value="2019" >2019</option>
						  <option value="2020" >2020</option>						  
						</select><br>
						<span class="text-danger"><?php if (isset($year_error)) echo $year_error; ?></span>	
					</div>


					<div class="form-group">
						<label for="name">Photo Upload</label>						
						    Select image to upload:
						    <input type="file" name="fileToUpload" id="fileToUpload">
						    <!-- <input type="submit" value="Upload Image" name="submit">-->
					</div>
				
					<div class="form-group">
						<input type="submit" name="signup" value="Create" class="btn btn-primary" />
					</div>
				</fieldset>
			</form>
			<span class="text-success"><?php if (isset($successmsg)) { echo $successmsg; } ?></span>
			<span class="text-danger"><?php if (isset($errormsg)) { echo $errormsg; } ?></span>
		</div>
	</div>
</div>
<script src="js/jquery-1.10.2.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>



