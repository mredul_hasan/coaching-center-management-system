<?php
ob_start();
session_start();
include_once 'dbconnect.php';
?>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Learn Center | Admissions</title>
<meta charset="utf-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<script type="text/javascript" src="js/jquery-1.5.2.js" ></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/cufon-replace.js"></script>
<script type="text/javascript" src="js/Molengo_400.font.js"></script>
<script type="text/javascript" src="js/Expletus_Sans_400.font.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<style type="text/css">.bg, .box2{behavior:url("js/PIE.htc");}</style>
<![endif]-->
<style>


li a, .dropbtn {
    display: inline-block;
    color: white;
    text-align: center;
    text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
    background-color: none;
}

li.dropdown {
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #008080;
    min-width: 10px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 2px 10px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdown-content a:hover {background-color: #696969}

.dropdown:hover .dropdown-content {
    display: block;
}
</style>


</head>
<body id="page5">
<div class="body1">
  <div class="main">
    <!-- header -->
    <header>
      <div class="wrapper">
        <nav>
          <ul id="menu">
            <li><a href="index.php">About</a></li>
            <li><a href="event.php">Event</a></li>
            <li class="dropdown">
              <a href="javascript:void(0)" class="dropbtn">Programs</a>
              <div class="dropdown-content">
                <a href="class_schedule.php">Class Schedule</a>
                <a href="exam_schedule.php">Exam Schedule</a>
                <a href="result.php">Result</a>
              </div>
            </li>
            <li><a href="teachers.php">Teachers</a></li>
            <li><a href="admissions.php">Admissions</a></li>
            <li class="end"><a href="contacts.php">Contacts</a></li>
          </ul>
        </nav>

        <ul id="menu"> 
        <?php if (isset($_SESSION['usr_id'])) { ?>        
            <li class="uName"><a><?php echo $_SESSION['usr_name']; ?></a></li>
            <li class="menu-item"><a href="login and registration form\logout.php">Log Out</a></li>   
          <?php } else { ?>            <li class="SignIn"><a href="login and registration form\login.php">Login</a></li>
            <li class="SignIn"><a href="login and registration form\register.php">Sign Up</a></li>
          <?php } ?>
        </ul>                        

      </div>
      <div class="wrapper">
        <h1><a href="index.php" id="logo">Learn Center</a></h1>
      </div>
      <div id="slogan"> We Will Open The World<span>of knowledge for you!</span> </div>

    </header>
    <!-- / header -->
  </div>
</div>
<div class="body2">
  <div class="main">
    <!-- content -->
    <section id="content">
      <div class="box1">
        <div class="wrapper">
          <article class="col1">                          
            <div class="pad_left1">
              <h2 class="pad_bot1">Our Mission</h2>
              <p class="pad_bot1 pad_top2"> We started our journey in 2005 established by Bash Deb Saha(Pintu). The aim of Ideal Coaching is to facilitate the preparation for taking schoole examination and board examination of Khulna, Bangladesh. In line with the result we have glorious and effective experience which is backed by Bash Deb Saha(Pintu) a prominent and pioneer person in coaching education system in Khulna, Bangladesh. Our vision is to become the distinguished and unique educational institution in Khulna, Bangladesh. Our mission is to provide the proper schooling and guidelines. Our strategy is to link between objectives, resources and priorities where resources includes the experiences of our teachers..,</p> 
            </div>
            
            <div class="pad_left1">
              <h2 class="pad_bot1">Terms of Admission</h2>
            </div>
            <div class="wrapper">              
              <p class="pad_top2">                           
              <ul class="listItem">

            <?php 
            $result = mysqli_query($connection, "SELECT `term` FROM admission WHERE term_id = 1");
            $row = mysqli_fetch_array($result);

            $term = $row['term'];            
            ?>
                <!-- <li class="listItem1"><?php echo $term; ?></li> -->

              <div>
                  <div>
                      <?php if (isset($_SESSION['usr_id'])) { ?>
                          <button type="button"  id="edit_btn" class = "myButton" style="float:right;">Edit!</button>
                      <?php } ?>
                  </div>
              </div>
              <ol id = "info">
                  <?php
                      echo "<h5>". $term."</h5>";
                  ?> 
              </ol>

               <ol id = "info2" >
                  <form action="about.php" id="usrform">
                    <textarea name="note_pad" rows="20" cols="120" class="textbox" id="note_pad" style='width: auto;'><?php echo $term; ?></textarea>
                    <input type="submit" name = "submit" class ="myButton" value="Update" style="float:right;">
                  </form>
              </ol>

            <?php 
            $result = mysqli_query($connection, "SELECT `term` FROM admission WHERE term_id = 2");
            $row = mysqli_fetch_array($result);

            $term = $row['term'];            
            ?>
                <li class="listItem1"><?php echo $term; ?></li>

            <?php 
            $result = mysqli_query($connection, "SELECT `term` FROM admission WHERE term_id = 3");
            $row = mysqli_fetch_array($result);

            $term = $row['term'];            
            ?>
                <li class="listItem1"><?php echo $term; ?></li>

            <?php 
            $result = mysqli_query($connection, "SELECT `term` FROM admission WHERE term_id = 4");
            $row = mysqli_fetch_array($result);

            $term = $row['term'];            
            ?>
                <li class="listItem1"><?php echo $term; ?></li>

            <?php 
            $result = mysqli_query($connection, "SELECT `term` FROM admission WHERE term_id = 5");
            $row = mysqli_fetch_array($result);

            $term = $row['term'];            
            ?>
                <li class="listItem1"><?php echo $term; ?></li>

             </ul>
              </p>
            </div>
            </article>
          <article class="col2 pad_left2">
            <div class="pad_left1">
              <h2>Important Dates</h2>
            </div>
          <div class="wrapper"> <span class="date">02</span>
              <p class="pad_top2"><a href="event.php">February, 2017</a><br>
                Start of SSC examination.</p>
            </div>
            <div class="wrapper"> <span class="date">25</span>
              <p class="pad_top2"><a href="event.php">March, 2017</a><br>
                1st model test for JSC examiner will be started.</p>
            </div>
            <div class="wrapper"> <span class="date">19</span>
              <p class="pad_top2"><a href="event.php">April, 2017</a><br>
              1st model test for PSC examiner will be started.</p>
            </div>
            <div class="wrapper"> <span class="date">18</span>
              <p class="pad_top2"><a href="event.php">May, 2017</a><br>
                Call of parents.</p>
            </div>
            
           </article>
           <article class="col2 pad_left2">
            <div class="pad_left1">
              <h2>New Programs</h2>
            </div>
            <ul class="list1">
              <li><a href="#">Special Batch For JSC </a></li>
              <li><a href="#">Special Batch For SSC</a></li>
              <li><a href="#">Weekly And Monthly Exam</a></li>
              <li><a href="#">Question Of Previous Exam</a></li>
              <li><a href="#">Marks Of Previous Exam</a></li>
            </ul>
            
             </article>

        </div>
      </div>
    </section>
    <!-- content -->
    <!-- footer -->
    <footer>
      <div class="wrapper">
        <div class="pad1">
          <div class="pad_left1">
            <div class="wrapper">
              <article class="col_1">

               <?php 
            $result = mysqli_query($connection, "SELECT * FROM basic_info");
            $row = mysqli_fetch_array($result);

            $email = $row['email'];
            $add = $row['adderss'];
            $road = $row['road'];
            $city = $row['city'];
            $country = $row['country'];
            ?>

                <h3>Address:</h3>
                <p class="col_address"><strong>Country:<br>
                  City:<br>                  
                  Road:<br>
                  Address:<br>
                  Email:</strong></p>
                <p><?php echo $country; ?><br>
                  <?php echo $city; ?><br>
                  <?php echo $road; ?><br>
                  <?php echo $add; ?><br>
                  <a href="#"><?php echo $email; ?></a></p>
              </article>
              <article class="col_2 pad_left2">
                <h3>Join In:</h3>
                <ul class="list2">
                  <li><a href="#"><img src="images/icon1.jpg" alt=""></a></li>
                  <li><a href="#"><img src="images/icon2.jpg" alt=""></a></li>
                </ul>
              </article>
              <article class="col_3 pad_left2">
                <h3>Why Us:</h3>
                <ul class="list2">
                  <li><a href="#">Our Mission Statement </a></li>
                  <li><a href="#">Performance Report</a></li>
                  <li><a href="#">Prospective Parents </a></li>                  
                </ul>
              </article>
              <article class="col_4 pad_left2">
                <h3>Newsletter:</h3>
                <form id="newsletter" action="#" method="post">
                  <div class="wrapper">
                    <div class="bg">
                      <input type="text">
                    </div>
                  </div>
                  <a href="#" class="button"><span><span><strong>Subscribe</strong></span></span></a>
                </form>
              </article>
            </div>
            <div class="wrapper">
              <article class="call"> <span class="call1">Call Us Now: </span><span class="call2">041-722983</span> </article>              
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- / footer -->
  </div>
</div>
<script type="text/javascript">Cufon.now();</script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
    $("#info2").hide();
    $(document).ready(function(){
        $("#edit_btn").click(function(){
            $("#info").hide();
            $("#edit_btn").hide();
            $("#info2").show();
        });
    });
    </script>
</body>
</html>