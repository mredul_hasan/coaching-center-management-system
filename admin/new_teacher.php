<?php
session_start();


include_once 'dbconnect.php';

//set validation error flag as false
$error = false;

//check if form is submitted
if (isset($_POST['signup'])) {
	$name = mysqli_real_escape_string($connection, $_POST['name']);
	$phone = mysqli_real_escape_string($connection, $_POST['phone']);
	$email = mysqli_real_escape_string($connection, $_POST['email']);
	$password = mysqli_real_escape_string($connection, $_POST['password']);
	$cpassword = mysqli_real_escape_string($connection, $_POST['cpassword']);
	$info = mysqli_real_escape_string($connection, $_POST['info']);
	$address = mysqli_real_escape_string($connection, $_POST['address']);
	$DOB = mysqli_real_escape_string($connection, $_POST['DOB']);
	$designation = mysqli_real_escape_string($connection, $_POST['designation']);
	$institution = mysqli_real_escape_string($connection, $_POST['institution']);
	
	
	//name can contain only alpha characters and space
	if (!preg_match("/^[a-zA-Z ]+$/",$name)) {
		$error = true;
		$name_error = "Name must contain only alphabets and space";
	}
	if(!preg_match('/^[0-9]{11}$/',$phone)) {
		$error = true;
		$phone_error = "Phone must contain only number";
	}	
	if(!filter_var($email,FILTER_VALIDATE_EMAIL)) {
		$error = true;
		$email_error = "Please Enter Valid Email ID";
	}
	if(strlen($password) < 6) {
		$error = true;
		$password_error = "Password must be minimum of 6 characters";
	}
	if($password != $cpassword) {
		$error = true;
		$cpassword_error = "Password and Confirm Password doesn't match";
	}

	
	
	//file upload
$target_dir = "images/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["signup"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 5000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    	
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";

    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

	if (!$error) {
		if(mysqli_query($connection, "INSERT INTO users(`user_name`, `email`, `password`,`last_login`) VALUES('" . $name . "', '" . $email . "','" . md5($password) . "',now())")) {

			$last_id = mysqli_query($connection, "SELECT user_id FROM users WHERE email = '" . $email. "' and password = '" . md5($password) . "' ");
			$row = mysqli_fetch_array($last_id);
			$last_id = $row['user_id'];
			
			mysqli_query($connection, "INSERT INTO `teacher`( `user_id`, `photo_path`, `birthday`, `address`, `phone_no`, `designation`, `institution`, `name`, `info`) VALUES('" . $last_id . "', 'admin/" . $target_file . "', '" . $DOB . "', '" . $address . "', '" . $phone . "', '" . $designation . "', '" . $institution . "', '" . $name . "', '" . $info . "')");
			$successmsg = "Successfully Registered!";
		} else {
			$errormsg = "Error in registering...Please try again later!";
		}
	}
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>User Registration Form</title>	
	<meta http-equiv="Content-Type" content="width=device-width, initial-scale=1.0" name="viewport" charset="UTF-8" />
	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
</head>
	<body>
		<header class="site-header">
			<div class="container">
				<a href="/wpl_1/about.php" id="branding">
					<img src="user.png" alt="" class="logo">
					<div class="logo-copy">
						<h1 class="site-title">Teacher Registration</h1>
						<small class="site-description">Computer Science &amp; Engineering Discipline</small>
					</div>
				</a> <!-- #branding -->				
			</div>
		</header>

<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4 well">
			<form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data" name="signupform">
				<fieldset>
					<legend>Sign Up</legend>

					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" name="name" placeholder="Enter Full Name" required value="<?php if($error) echo $name; ?>" class="form-control" />
						<span class="text-danger"><?php if (isset($name_error)) echo $name_error; ?></span>
					</div>
					
					<div class="form-group">
						<label for="name">Email</label>
						<input type="text" name="email" placeholder="Email" required value="<?php if($error) echo $email; ?>" class="form-control" />
						<span class="text-danger"><?php if (isset($email_error)) echo $email_error; ?></span>
					</div>

					<div class="form-group">
						<label for="name">Phone No</label>
						<input type="text" name="phone" placeholder="Enter phone no" required value="<?php if($error) echo $phone; ?>" class="form-control" />
						<span class="text-danger"><?php if (isset($phone_error)) echo $phone_error; ?></span>
					</div>

					<div class="form-group">
						<label for="name">Information</label>
						<input type="text" name="info" placeholder="Write somthing about this person" required value="<?php if($error) echo $info; ?>" class="form-control" />
						<!-- <span class="text-danger"><?php if (isset($info_error)) echo $info_error; ?></span> -->
					</div>

					<div class="form-group">
						<label for="name">Address</label>
						<input type="text" name="address" placeholder="Enter address" required value="<?php if($error) echo $address; ?>" class="form-control" />
						<!-- <span class="text-danger"><?php if (isset($info_error)) echo $info_error; ?></span> -->
					</div>
					
					<div class="form-group">
						<label for="name">Designation</label>
						<input type="text" name="designation" placeholder="Enter designation" required value="<?php if($error) echo $designation; ?>" class="form-control" />
						<span class="text-danger"><?php if (isset($designation_error)) echo $designation_error; ?></span>					
					</div>
					
					<div class="form-group">
						<label for="name">Institution</label>
						<input type="text" name="institution" placeholder="Enter institution" required value="<?php if($error) echo $institution; ?>" class="form-control" />
						<span class="text-danger"><?php if (isset($institution_error)) echo $institution_error; ?></span>					
					</div>
					
					<div class="form-group">
						<label for="name">Password</label>
						<input type="password" name="password" placeholder="Password" required class="form-control" />
						<span class="text-danger"><?php if (isset($password_error)) echo $password_error; ?></span>
					</div>

					<div class="form-group">
						<label for="name">Confirm Password</label>
						<input type="password" name="cpassword" placeholder="Confirm Password" required class="form-control" />
						<span class="text-danger"><?php if (isset($cpassword_error)) echo $cpassword_error; ?></span>
					</div>
									
					<div class="form-group">
						<label for="name">Date of birth</label>						
						<input type="date" name="DOB" placeholder="yyyy-mm-dd" pattern="\d{4}-\d{1,2}-\d{1,2}"><br>
					</div>

					<div class="form-group">
						<label for="name">Photo Upload</label>						
						    Select image to upload:
						    <input type="file" name="fileToUpload" id="fileToUpload">
						    <!-- <input type="submit" value="Upload Image" name="submit">						 -->
					</div>
				
					<div class="form-group">
						<input type="submit" name="signup" value="Sign Up" class="btn btn-primary" />
					</div>
				</fieldset>
			</form>
			<span class="text-success"><?php if (isset($successmsg)) { echo $successmsg; } ?></span>
			<span class="text-danger"><?php if (isset($errormsg)) { echo $errormsg; } ?></span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-md-offset-4 text-center">	
		Already Registered? <a href="login.php">Login Here</a>
		</div>
	</div>
</div>
<script src="js/jquery-1.10.2.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>



