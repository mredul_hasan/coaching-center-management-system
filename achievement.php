<?php
ob_start();
session_start();
include_once 'dbconnect.php';

$sql = "SELECT * FROM `event` ";
$event = mysqli_query($connection, $sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Learn Center | Achievements</title>
<meta charset="utf-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">

<script type="text/javascript" src="js/jquery-1.5.2.js" ></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/cufon-replace.js"></script>
<script type="text/javascript" src="js/Molengo_400.font.js"></script>
<script type="text/javascript" src="js/Expletus_Sans_400.font.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<style type="text/css">.bg, .box2{behavior:url("js/PIE.htc");}</style>
<![endif]-->
</head>
<body id="page2">
<div class="body1">
  <div class="main">
    <!-- header -->
    <header>
      <div class="wrapper">
        <nav>
          <ul id="menu">
            <li><a href="index.php">About</a></li>
            <li><a href="event.php?id=1">Event</a></li>
            <li><a href="programs.php">Programs</a></li>
            <li><a href="teachers.php">Teachers</a></li>
            <li><a href="admissions.php">Admissions</a></li>
            <li class="end"><a href="contacts.php">Contacts</a></li>
          </ul>
        </nav>

        <ul id="menu"> 
        <?php if (isset($_SESSION['usr_id'])) { ?>        
            <li class="uName"><a><?php echo $_SESSION['usr_name']; ?></a></li>
            <li class="menu-item"><a href="login and registration form\logout.php">Log Out</a></li>   
          <?php } else { ?> <li class="SignIn"><a href="login and registration form\login.php">Login</a></li>
            <li class="SignIn"><a href="login and registration form\register.php">Sign Up</a></li>
          <?php } ?>
        </ul>                        

      </div>
      <div class="wrapper">
        <h1><a href="index.php" id="logo">Learn Center</a></h1>
      </div>
      <div id="slogan"> We Will Open The World<span>of knowledge for you!</span> </div>

    </header>
    <!-- / header -->
  </div>
</div>
<div class="body2">
  <div class="main">
    <!-- content -->
    <div id="content">     
      <main class="main-content">
        <div class="container">
          <div class="page">
            <div class="filters">
              <select name="#" class="dropdown" placeholder="Choose Category">
                <option value="#">PSC</option>
                <option value="#">JSC</option>
                <option value="#">SSC</option>                          
              </select>
              <select name="#" class="dropdown">                
                <option value="#">2010</option>
                <option value="#">2011</option>
                <option value="#">2012</option>
                <option value="#">2013</option>
                <option value="#">2014</option>
                <option value="#">2015</option>
                <option value="#">2016</option>
                <option value="#">2017</option>
              </select>
            </div>
            <style type="text/css">
              .dropdown {
                  position: relative;
                  display: inline-block;
                  min-width: 160px;
                  padding: 12px 16px;
                  background-color: royalblue;
              }
            </style>
            <br>
            <div class="movie-list">
              <div class="movie">
                <figure class="movie-poster"><img src="dummy/PSC-Exam-Results-2015.jpg" alt="#"></figure>
                <div class="movie-title">PSC 2015</div>
                <p>Glorious performance from the kids in PSC 2015 exam.</p>
              </div>
              <div class="movie">
                <figure class="movie-poster"><img src="dummy/PSC-Exam-Results-2016.jpg" alt="#"></figure>
                <div class="movie-title">PSC 2016</div>
                <p>Hilarious performance from our children again.</p>
              </div>
              <div class="movie">
                <figure class="movie-poster"><img src="dummy/JSC-exam-result-2016.jpg" alt="#"></figure>
                <div class="movie-title">JSC 2016</div>
                <p>Remarkable result from ideal' coaching center in junior division this year.</p>
              </div>
              <div class="movie">
                <figure class="movie-poster"><img src="dummy/JSC-exam-result-2017.jpg" alt="#"></figure>
                <div class="movie-title">JSC 2017</div>
                <p>Best divitional result in khulna this year from ideal coaching center.</p>
              </div>
              <div class="movie">
                <figure class="movie-poster"><img src="dummy/ssc-result-2015-bd.jpg" alt="#"></figure>
                <div class="movie-title">SSC 2015</div>
                <p>5 talents topped in jessore board from ideal coaching center out of 20.</p>
              </div>
              <div class="movie">
                <figure class="movie-poster"><img src="dummy/ssc-result-2016-bd.jpg" alt="#"></figure>
                <div class="movie-title">SSC 2016</div>
                <p>Idealian sets new milestone in SSC exam.</p>
              </div>              
             </div>
            </div> <!-- .movie-list -->           
          </div>
        </div> <!-- .container -->
      </main>     
    </div>
    <!-- content -->
    <!-- footer -->
    <footer>
      <div class="wrapper">
        <div class="pad1">
          <div class="pad_left1">
            <div class="wrapper">
              <article class="col_1">

            <?php
              $result = mysqli_query($connection, "SELECT * FROM basic_info");
              $row = mysqli_fetch_array($result);

              $email = $row['email'];
              $add = $row['adderss'];
              $road = $row['road'];
              $city = $row['city'];
              $country = $row['country'];
            ?>

                <h3>Address:</h3>
                <p class="col_address"><strong>Country:<br>
                  City:<br>                  
                  Road:<br>
                  Address:<br>
                  Email:</strong></p>
                <p><?php echo $country; ?><br>
                  <?php echo $city; ?><br>
                  <?php echo $road; ?><br>
                  <?php echo $add; ?><br>
                  <a href="#"><?php echo $email; ?></a></p>
              </article>
              <article class="col_2 pad_left2">
                <h3>Join In:</h3>
                <ul class="list2">
                  <li><a href="#"><img src="images/icon1.jpg" alt=""></a></li>
                  <li><a href="#"><img src="images/icon2.jpg" alt=""></a></li>
                </ul>
              </article>
              <article class="col_3 pad_left2">
                <h3>Why Us:</h3>
                <ul class="list2">
                  <li><a href="#">Our Mission Statement </a></li>
                  <li><a href="#">Performance Report</a></li>
                  <li><a href="#">Prospective Parents </a></li>                  
                </ul>
              </article>
              <article class="col_4 pad_left2">
                <h3>Newsletter:</h3>
                <form id="newsletter" action="#" method="post">
                  <div class="wrapper">
                    <div class="bg">
                      <input type="text">
                    </div>
                  </div>
                  <a href="#" class="button"><span><span><strong>Subscribe</strong></span></span></a>
                </form>
              </article>
            </div>
            <div class="wrapper">
              <article class="call"> <span class="call1">Call Us Now: </span><span class="call2">041-722983</span> </article>              
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- / footer -->
  </div>
</div>
<script src="js/js/jquery-1.11.1.min.js"></script>
<script src="js/js/plugins.js"></script>
<script src="js/js/app.js"></script>
<script src="js/app.js"></script>
</body>
</html>