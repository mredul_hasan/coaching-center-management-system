-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2018 at 05:56 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `coaching_institution`
--

-- --------------------------------------------------------

--
-- Table structure for table `achieve`
--

CREATE TABLE IF NOT EXISTS `achieve` (
  `achievement_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`achievement_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `achievement`
--

CREATE TABLE IF NOT EXISTS `achievement` (
  `achievement_id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `description` text NOT NULL,
  `photo_path` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`achievement_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `achievement`
--

INSERT INTO `achievement` (`achievement_id`, `year`, `description`, `photo_path`, `title`) VALUES
(1, 2017, 'Hilarious performance from our children again.', 'admin/images/SSC exam.jpg', 'PSC 2016'),
(2, 2017, 'Hilarious performance from our children again.', 'admin/images/', ''),
(3, 2017, 'Hilarious performance from our children again.', 'admin/images/DSC05058.JPG', 'PSC 2017');

-- --------------------------------------------------------

--
-- Table structure for table `admission`
--

CREATE TABLE IF NOT EXISTS `admission` (
  `term_id` int(11) NOT NULL AUTO_INCREMENT,
  `term` varchar(200) NOT NULL,
  PRIMARY KEY (`term_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `admission`
--

INSERT INTO `admission` (`term_id`, `term`) VALUES
(1, 'Student must be attentive and participate in all examination.'),
(2, 'They must be pay there monthly fees in time.'),
(3, 'They have to be follow all the rules and regulations of IDEAL COACHING.'),
(4, 'They should not late in their class.'),
(5, 'If they break any rule, then fine is applied and inform their guardians in required.');

-- --------------------------------------------------------

--
-- Table structure for table `basic_info`
--

CREATE TABLE IF NOT EXISTS `basic_info` (
  `email` varchar(50) NOT NULL,
  `adderss` varchar(100) NOT NULL,
  `road` varchar(100) NOT NULL,
  `city` varchar(30) NOT NULL,
  `country` varchar(30) NOT NULL,
  `phone_no` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `basic_info`
--

INSERT INTO `basic_info` (`email`, `adderss`, `road`, `city`, `country`, `phone_no`) VALUES
('ideal82@yahoo.com', 'Moulavipara,Khulna', '13,Sultan Ahmed Road', 'KHULNA', 'BANGLADESH', '');

-- --------------------------------------------------------

--
-- Table structure for table `class_schedule`
--

CREATE TABLE IF NOT EXISTS `class_schedule` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(12) NOT NULL,
  `time` time NOT NULL,
  KEY `course_id` (`subject_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `class_schedule`
--

INSERT INTO `class_schedule` (`subject_id`, `day`, `time`) VALUES
(1, 'saturday', '09:00:00'),
(2, 'saturday', '10:00:00'),
(3, 'saturday', '11:00:00'),
(4, 'sunday', '09:00:00'),
(5, 'sunday', '10:00:00'),
(6, 'sunday', '11:00:00'),
(7, 'monday', '09:00:00'),
(8, 'monday', '10:00:00'),
(9, 'monday', '11:00:00'),
(10, 'tuesday', '09:00:00'),
(11, 'tuesday', '10:00:00'),
(12, 'tuesday', '11:00:00'),
(13, 'wednessday', '09:00:00'),
(1, 'wednessday', '10:00:00'),
(2, 'wednessday', '11:00:00'),
(3, 'thursday', '09:00:00'),
(4, 'thursday', '10:00:00'),
(5, 'thursday', '11:00:00'),
(14, 'saturday', '09:00:00'),
(15, 'saturday', '10:00:00'),
(16, 'saturday', '11:00:00'),
(17, 'sunday', '09:00:00'),
(18, 'sunday', '10:00:00'),
(19, 'sunday', '11:00:00'),
(14, 'monday', '09:00:00'),
(15, 'monday', '10:00:00'),
(15, 'monday', '11:00:00'),
(16, 'tuesday', '09:00:00'),
(17, 'tuesday', '10:00:00'),
(18, 'tuesday', '11:00:00'),
(19, 'wednessday', '09:00:00'),
(14, 'wednessday', '10:00:00'),
(15, 'wednessday', '11:00:00'),
(16, 'thursday', '09:00:00'),
(17, 'thursday', '10:00:00'),
(18, 'thursday', '11:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` text NOT NULL,
  `details` text NOT NULL,
  `photo_path` varchar(100) NOT NULL,
  `date` int(11) NOT NULL,
  `month` varchar(20) NOT NULL,
  `year` int(11) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `headline`, `details`, `photo_path`, `date`, `month`, `year`) VALUES
(1, 'SSC Examinaiton,2017', 'The Secondary School Certificate (SSC) and equivalent examinations are about begin with 14,32,727 examinees taking part from 27,489 educational institutions across the country. This year, the number of examinees rose by more than 1.29 lakh. However, over 1.66 lakh students are not sitting for the exams as they did not fill in the forms. They got registered in class IX as regular students. At a pre-examination press briefing at the secretariat yesterday, Education Minister Nurul Islam Nahid said 7,33,202 boys and 6,99,525 girls will appear in the SSC and equivalent examinations at 2,942 centres under 10 education boards. Of them, more than 10.90 lakh students will sit for the SSC exams under eight general education boards. Some 2.39 lakh students will take Dakhil (secondary level of madrasa) examinations and around 1.02 lakh SSC vocational exams, he said. The candidates will take Bangla First Paper tests on the first day. The exams would end on March 20 with the practical exams beginning on March 25,2017.', 'images\\SSC exam.jpg', 25, 'March', 2012),
(2, 'SSC Result,2017', 'The Secondary School Certificate (SSC) and equivalent examinations are about begin with 14,32,727 examinees taking part from 27,489 educational institutions across the country. This year, the number of examinees rose by more than 1.29 lakh. However, over 1.66 lakh students are not sitting for the exams as they did not fill in the forms. They got registered in class IX as regular students. At a pre-examination press briefing at the secretariat yesterday, Education Minister Nurul Islam Nahid said 7,33,202 boys and 6,99,525 girls will appear in the SSC and equivalent examinations at 2,942 centres under 10 education boards. Of them, more than 10.90 lakh students will sit for the SSC exams under eight general education boards. Some 2.39 lakh students will take Dakhil (secondary level of madrasa) examinations and around 1.02 lakh SSC vocational exams, he said. The candidates will take Bangla First Paper tests on the first day. The exams would end on March 20 with the practical exams beginning on March 25,2017.', 'images\\ssc result.jpg', 11, 'April', 2012),
(3, 'JSC', 'Jsc exam', 'images\\page3_img3.jpg', 15, 'March', 2017),
(4, 'SSC Result,2017 test', 'The Secondary School Certificate (SSC) and equivalent examinations are about begin with 14,32,727 examinees taking part from 27,489 educational institutions across the country. This year, the number of examinees rose by more than 1.29 lakh. However, over 1.66 lakh students are not sitting for the exams as they did not fill in the forms. They got registered in class IX as regular students. At a pre-examination press briefing at the secretariat yesterday, Education Minister Nurul Islam Nahid said 7,33,202 boys and 6,99,525 girls will appear in the SSC and equivalent examinations at 2,942 centres under 10 education boards. Of them, more than 10.90 lakh students will sit for the SSC exams under eight general education boards. Some 2.39 lakh students will take Dakhil (secondary level of madrasa) examinations and around 1.02 lakh SSC vocational exams, he said. The candidates will take Bangla First Paper tests on the first day. The exams would end on March 20 with the practical exams beginning on March 25,2017.', 'images\\alamgir.png', 14, 'February', 2017),
(5, 'SSC Result,2017 new', 'The Secondary School Certificate (SSC) and equivalent examinations are about begin with 14,32,727 examinees taking part from 27,489 educational institutions across the country. This year, the number of examinees rose by more than 1.29 lakh. However, over 1.66 lakh students are not sitting for the exams as they did not fill in the forms. They got registered in class IX as regular students. At a pre-examination press briefing at the secretariat yesterday, Education Minister Nurul Islam Nahid said 7,33,202 boys and 6,99,525 girls will appear in the SSC and equivalent examinations at 2,942 centres under 10 education boards. Of them, more than 10.90 lakh students will sit for the SSC exams under eight general education boards. Some 2.39 lakh students will take Dakhil (secondary level of madrasa) examinations and around 1.02 lakh SSC vocational exams, he said. The candidates will take Bangla First Paper tests on the first day. The exams would end on March 20 with the practical exams beginning on March 25,2017.', 'admin/images/anis.png', 12, 'February', 2010),
(6, 'SSC Result,2017 test new', 'The Secondary School Certificate (SSC) and equivalent examinations are about begin with 14,32,727 examinees taking part from 27,489 educational institutions across the country. This year, the number of examinees rose by more than 1.29 lakh. However, over 1.66 lakh students are not sitting for the exams as they did not fill in the forms. They got registered in class IX as regular students. At a pre-examination press briefing at the secretariat yesterday, Education Minister Nurul Islam Nahid said 7,33,202 boys and 6,99,525 girls will appear in the SSC and equivalent examinations at 2,942 centres under 10 education boards. Of them, more than 10.90 lakh students will sit for the SSC exams under eight general education boards. Some 2.39 lakh students will take Dakhil (secondary level of madrasa) examinations and around 1.02 lakh SSC vocational exams, he said. The candidates will take Bangla First Paper tests on the first day. The exams would end on March 20 with the practical exams beginning on March 25,2017.', 'admin/images/amit.png', 6, 'June', 2017),
(7, 'SSC Result,2017 test', 'nothing', 'admin//imagestour.png', 3, 'January', 2017),
(8, 'SSC Result,2017 test new', 'somthing', 'learn-centerimagestour.png', 6, 'April', 2017);

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE IF NOT EXISTS `exam` (
  `exam_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `question_path` varchar(100) NOT NULL,
  PRIMARY KEY (`exam_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exam`
--

INSERT INTO `exam` (`exam_id`, `date`, `question_path`) VALUES
(1, '2017-03-05 10:30:00', 'question/Bangla-1st-Paper_SSC-2016_0.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `exam_schedule`
--

CREATE TABLE IF NOT EXISTS `exam_schedule` (
  `subject_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `e_day` varchar(12) NOT NULL,
  KEY `course_id` (`subject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_schedule`
--

INSERT INTO `exam_schedule` (`subject_id`, `date`, `time`, `e_day`) VALUES
(1, '2017-03-01', '10:00:00', 'wednessday'),
(2, '2017-03-02', '10:00:00', 'thursday'),
(3, '2017-03-04', '09:00:00', 'saturday'),
(4, '2017-03-05', '08:00:00', 'sunday'),
(5, '2017-03-06', '11:00:00', 'monday'),
(6, '2017-03-07', '11:00:00', 'tuesday'),
(7, '2017-03-08', '10:00:00', 'wednessday'),
(8, '2017-03-09', '11:00:00', 'thursday'),
(9, '2017-03-11', '09:30:00', 'saturday'),
(10, '2017-03-12', '10:00:00', 'sunday'),
(11, '2017-03-13', '10:30:00', 'monday'),
(12, '2017-03-14', '10:00:00', 'tuesday'),
(13, '2017-03-15', '10:00:00', 'wednessday'),
(14, '2017-03-16', '10:30:00', 'thursday'),
(15, '2017-03-18', '08:45:00', 'saturday'),
(16, '2017-03-19', '09:00:00', 'sunday'),
(17, '2017-03-20', '09:00:00', 'monday'),
(18, '2017-03-21', '09:00:00', 'tuesday'),
(19, '2017-03-22', '09:30:00', 'wednessday');

-- --------------------------------------------------------

--
-- Table structure for table `marks`
--

CREATE TABLE IF NOT EXISTS `marks` (
  `student_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `value` tinyint(4) NOT NULL,
  KEY `student_id` (`student_id`),
  KEY `exam_id` (`exam_id`),
  KEY `course_id` (`subject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marks`
--

INSERT INTO `marks` (`student_id`, `exam_id`, `subject_id`, `value`) VALUES
(1, 1, 1, 87),
(2, 1, 1, 89),
(3, 1, 1, 92),
(4, 1, 1, 86),
(5, 1, 1, 84);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `roll_no` int(11) NOT NULL,
  `photo_path` varchar(100) NOT NULL,
  `birthday` date NOT NULL,
  `class` tinyint(4) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone_no` varchar(20) NOT NULL,
  `guardian_phone_no` varchar(20) NOT NULL,
  `institution` varchar(100) NOT NULL,
  `guardian` varchar(50) NOT NULL,
  PRIMARY KEY (`student_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_id`, `user_id`, `roll_no`, `photo_path`, `birthday`, `class`, `address`, `phone_no`, `guardian_phone_no`, `institution`, `guardian`) VALUES
(1, 1, 140241, 'images\\page4_img3.jpg', '1997-02-14', 8, 'Nirala,Khulna', '01988888876', '01988888876', 'Khulna Public College', 'MD. Abdul Motin'),
(2, 2, 140238, 'images\\girl-with-book1.jpg', '1996-02-08', 8, 'Kishoregonj', '01900000000', '01900000000', 'Khulna poilot school', 'Enamul Haq'),
(3, 3, 140222, 'images\\student.jpg', '1995-02-07', 8, 'Terokhada,Khulna', '01988888876', '01685882861', 'Model School,Khulna', 'Sobuj Kumar Saha'),
(4, 4, 2, 'C:\\xampp\\htdocs\\learn-center\\images\\page4_img3.jpg', '1997-02-14', 8, 'Mirzapur,Khulna', '01900000000', '01900000000', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `student_payment`
--

CREATE TABLE IF NOT EXISTS `student_payment` (
  `pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `month` varchar(10) NOT NULL,
  `paid` int(11) NOT NULL,
  `due` int(11) NOT NULL,
  `pay_date` date NOT NULL,
  PRIMARY KEY (`pay_id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `student_payment`
--

INSERT INTO `student_payment` (`pay_id`, `student_id`, `month`, `paid`, `due`, `pay_date`) VALUES
(1, 1, 'March', 3001, 2001, '2017-03-23'),
(2, 3, 'April', 4000, 1000, '2017-04-02');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `class` tinyint(4) NOT NULL,
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`subject_id`, `sub_name`, `class`) VALUES
(1, 'Bangla 1st Paper', 8),
(2, 'Bangla 2nd Paper', 8),
(3, 'English 1st Paper', 8),
(4, 'English 2nd Paper', 8),
(5, 'Religion', 8),
(6, 'Information & Communication Science', 8),
(7, 'Introduce Of Bangladesh And World', 8),
(8, 'Physical Education & Health', 8),
(9, 'Genaral math', 8),
(10, 'Work and Life Oriented Education', 8),
(11, 'General Science', 8),
(12, 'Arts and crafts', 8),
(13, 'Agriculture Education/Home Science', 8),
(14, 'English', 5),
(15, 'Bangla', 5),
(16, 'Introduce Of Bangladesh And World', 5),
(17, 'Primary Science', 5),
(18, 'Religion', 5),
(19, 'Mathematics', 5);

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE IF NOT EXISTS `teacher` (
  `teacher_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `photo_path` varchar(100) NOT NULL,
  `birthday` date NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone_no` varchar(20) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `institution` varchar(100) NOT NULL,
  `name` varchar(30) NOT NULL,
  `info` text NOT NULL,
  PRIMARY KEY (`teacher_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`teacher_id`, `user_id`, `photo_path`, `birthday`, `address`, `phone_no`, `designation`, `institution`, `name`, `info`) VALUES
(1, 4, 'images\\student.jpg', '1987-02-08', 'Khulna', '01900000000', 'Lecturer', 'Khulna Collegiate Girls''s School', 'Muksedul Islam Mishuk', 'If you are confused about grammar, what are you waiting for? It''s your time to learn!If you''re insecure about grammar or writing,I urge you to spend some time learning.You''re smart. You can do it!'),
(2, 5, 'images\\page4_img2.jpg', '1987-02-08', 'Khulna', '01700000000', 'Math Teacher', 'Model School,Khulna', 'Juel Hossain', 'Anyone can learn math whether they''re in higher math at school or just looking to brush up on the basics. After discussing ways to be a good math student, this article will teach you the basic progression of math courses and will give you the basic elements that you''ll need to learn in each course. Then, the article will go through the basics of learning arithmetic, which will help both kids in elementary school and anyone else who needs to brush up on the fundamentals..'),
(3, 1, 'images\\page4_img3.jpg', '1987-02-08', 'Khulna', '01800000000', 'Physics Teacher', 'Khulna Public College', 'Sk Rahad Mannan', 'You learn more physics by studying it for an hour a day than by studying it for ten hours on a week end, and it takes less time. Furthermore, you will get more from the middle-of-the-week classes Don''t get behind. Keep up with your work. It''s much easier to learn your lessons from day to day than it is to half-learn them all at once on the day before the exam. If the prospect of an assignment is forbidding, begin on it; you may get more done than you expected...'),
(4, 1, 'images\\images.jpg', '1987-02-08', 'Khulna', '01500000000', 'Student,BBA Discipline', 'Khulna University', 'Roshni Alom', 'Some of the basic accounting terms that you will learn include revenues, expenses, assets, liabilities, income statement, balance sheet, and statement of cash flows. You will become familiar with accounting debits and credits as we show you how to record transactions. You will also see why two basic accounting principles, the revenue recognition principle and the matching principle, assure that a company''s income statement reports a company''s profitability...'),
(5, 0, 'images/amit.png', '0000-00-00', 'Khulna', '01929326055', 'Lecturer', 'Khulna University', 'somthing', ''),
(6, 0, 'images/anis.png', '1994-12-12', 'Khulna', '01929326055', 'Lecturer', 'Khulna University', 'Rafa', 'nothing'),
(7, 9, 'images/anis1.png', '1994-12-12', 'Khulna', '01929326055', 'Assistant Professor', 'Khulna University', 'Rafa', 'nothing'),
(8, 10, 'admin/images/anupom.png', '1994-12-12', 'Bagherhat', '01929326055', 'Associate Professor', 'Khulna University', 'Rumpa', 'damn lol :v'),
(9, 11, 'admin/images/', '0000-00-00', '', '', '', '', '', ''),
(10, 11, 'admin/images/', '0000-00-00', '', '', '', '', '', ''),
(11, 11, 'admin/images/', '0000-00-00', '', '', '', '', '', ''),
(12, 14, 'admin/images/DSC05086.JPG', '0000-00-00', 'Bagherhat', '01929326055', '', '', 'farhana', 'damn lol :v');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_payment`
--

CREATE TABLE IF NOT EXISTS `teacher_payment` (
  `pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL,
  `month` varchar(10) NOT NULL,
  `paid` int(11) NOT NULL,
  `due` int(11) NOT NULL,
  `pay_date` date NOT NULL,
  PRIMARY KEY (`pay_id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `teacher_payment`
--

INSERT INTO `teacher_payment` (`pay_id`, `teacher_id`, `month`, `paid`, `due`, `pay_date`) VALUES
(1, 4, 'March', 8000, 2000, '2017-03-02'),
(2, 5, 'April', 7000, 3000, '2017-04-02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_type` varchar(12) NOT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `email`, `password`, `user_type`, `last_login`) VALUES
(1, 'Tania', 'Tania@gmail.com', '04b9b2b7a77b2ceb274548acbbae1755', 'admin', '2017-02-22 13:36:26'),
(2, 'mredul', 'mredul@gmail.com', '62a5575cc3f63d9d98e085a395e60599', 'admin', '2017-04-08 16:08:00'),
(3, 'sumit', 'sumit@gmail.com', '4f6ff51f541e12e548fe01318f01d382', 'admin', '2017-04-12 02:55:35'),
(4, 'mishuk', 'mishuk@gmail.com', 'f474dff115cd4be8dad153b0d5702ad2', '', '2017-03-22 03:38:21'),
(5, 'subroto', 'subroto@gmail.com', 'c18ca4d71f88b3d129d0f1997387da35', '', '0000-00-00 00:00:00'),
(6, 'somthing', 'Rafa1@gmail.com', '76938710cff96371247fe71fb85fcacd', '', '2017-04-08 03:36:49'),
(7, 'somthing', 'Rafa1@gmail.com', '76938710cff96371247fe71fb85fcacd', '', '2017-04-08 03:56:36'),
(8, 'Rafa', 'Rafa1@gmail.com', '76938710cff96371247fe71fb85fcacd', '', '2017-04-08 04:08:17'),
(9, 'Rafa', 'Rafa@gmail.com', '76938710cff96371247fe71fb85fcacd', '', '2017-04-08 04:10:23'),
(10, 'Rumpa', 'Rumpa@gmail.com', '158abdf808d296bf5d37374a27f0a125', '', '2017-04-08 04:13:57'),
(11, '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '2017-04-08 04:19:32'),
(12, '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '2017-04-08 04:19:54'),
(13, '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '2017-04-08 04:20:09'),
(14, 'farhana', 'farhana@gmail.com', 'f6c29ea92aab180183100dff95cce70a', '', '2017-04-10 00:31:44');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
