<?php
ob_start();
session_start();
include_once 'dbconnect.php';

$sql = "SELECT student.roll_no, student.class, users.user_name, marks.value, exam.date, exam.question_path, subject.sub_name FROM student, marks, exam, subject, users WHERE users.user_id = student.user_id AND student.student_id = marks.student_id AND marks.exam_id = exam.exam_id and marks.subject_id = subject.subject_id AND student.class = subject.class ORDER BY student.roll_no";

$result = mysqli_query($connection, $sql);


$sql2 = "SELECT student.class, exam.date, exam.question_path, subject.sub_name FROM student, marks, exam, subject, users WHERE users.user_id = student.user_id AND student.student_id = marks.student_id AND marks.exam_id = exam.exam_id and marks.subject_id = subject.subject_id AND student.class = subject.class ORDER BY student.roll_no";

$res = mysqli_query($connection, $sql2);
$exam_info = mysqli_fetch_array($res);

$class = $exam_info['class'];
$date = $exam_info['date'];
$question = $exam_info['question_path'];
$sub = $exam_info['sub_name'];


?>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Learn Center | Result</title>
<meta charset="utf-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<script type="text/javascript" src="js/jquery-1.5.2.js" ></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/cufon-replace.js"></script>
<script type="text/javascript" src="js/Molengo_400.font.js"></script>
<script type="text/javascript" src="js/Expletus_Sans_400.font.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<style type="text/css">.bg, .box2{behavior:url("js/PIE.htc");}</style>
<![endif]-->
<style>


li a, .dropbtn {
    display: inline-block;
    color: white;
    text-align: center;
    text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
    background-color: none;
}

li.dropdown {
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #008080;
    min-width: 10px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 2px 10px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdown-content a:hover {background-color: #696969}

.dropdown:hover .dropdown-content {
    display: block;
}
</style>


</head>
<body id="page5">
<div class="body1">
  <div class="main">
    <!-- header -->
    <header>
      <div class="wrapper">
        <nav>
          <ul id="menu">
            <li><a href="index.php">About</a></li>
            <li><a href="event.php">Event</a></li>
            <li class="dropdown">
              <a href="javascript:void(0)" class="dropbtn">Programs</a>
              <div class="dropdown-content">
                <a href="class_schedule.php">Class Schedule</a>
                <a href="exam_schedule.php">Exam Schedule</a>
                <a href="result.php">Result</a>
              </div>
            </li>
            <li><a href="teachers.php">Teachers</a></li>
            <li><a href="admissions.php">Admissions</a></li>
            <li class="end"><a href="contacts.php">Contacts</a></li>
          </ul>
        </nav>

        <ul id="menu"> 
        <?php if (isset($_SESSION['usr_id'])) { ?>        
            <li class="uName"><a><?php echo $_SESSION['usr_name']; ?></a></li>
            <li class="menu-item"><a href="login and registration form\logout.php">Log Out</a></li>   
          <?php } else { ?> <li class="SignIn"><a href="login and registration form\login.php">Login</a></li>
            <li class="SignIn"><a href="login and registration form\register.php">Sign Up</a></li>
          <?php } ?>
        </ul>                        

      </div>
      <div class="wrapper">
        <h1><a href="index.php" id="logo">Learn Center</a></h1>
      </div>
      <div id="slogan"> We Will Open The World<span>of knowledge for you!</span> </div>

    </header>
    <!-- / header -->
  </div>
</div>
<div class="body2">
  <div class="main">
    <!-- content -->
    <section id="content">
      <div class="box1">
        <div class="wrapper">
          
          <table id = 't01'>
            <caption>Class: <?php echo $class ;?>          Date: <?php echo $date ;?></caption>
            <tr><th>ROLL NO</th><th>NAME</th><th><?php echo $sub ;?></th></tr>

            <?php

              while($row = mysqli_fetch_assoc($result))
              {
                  echo "<tr>";
                  echo "<td>". $row['roll_no']."</td>"."<td>". $row["user_name"]."</td>"."<td>". $row["value"]."</td>";
                  echo "</tr>";
              }

            ?>

          </table>

      <h1>Question Paper</h1>
          
        </div>
      </div>
    </section>
    <!-- content -->

      


     <style>
        caption {
          font-size: 20px;
          color: #1aa3ff;
        }
        table {
            width:100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
            text-align: left;
            color: green;
        }
        table#t01 tr:nth-child(even) {
            background-color: #eee;
        }
        table#t01 tr:nth-child(odd) {
           background-color:#fff;
        }
        table#t01 th {
            background-color: #1affff;
            color: white;
        }
      </style>

    <!-- footer -->
    <footer>
      <div class="wrapper">
        <div class="pad1">
          <div class="pad_left1">
            <div class="wrapper">
              <article class="col_1">

               <?php 
            $result = mysqli_query($connection, "SELECT * FROM basic_info");
            $row = mysqli_fetch_array($result);

            $email = $row['email'];
            $add = $row['adderss'];
            $road = $row['road'];
            $city = $row['city'];
            $country = $row['country'];
            ?>

                <h3>Address:</h3>
                <p class="col_address"><strong>Country:<br>
                  City:<br>                  
                  Road:<br>
                  Address:<br>
                  Email:</strong></p>
                <p><?php echo $country; ?><br>
                  <?php echo $city; ?><br>
                  <?php echo $road; ?><br>
                  <?php echo $add; ?><br>
                  <a href="#"><?php echo $email; ?></a></p>
              </article>
              <article class="col_2 pad_left2">
                <h3>Join In:</h3>
                <ul class="list2">
                  <li><a href="#"><img src="images/icon1.jpg" alt=""></a></li>
                  <li><a href="#"><img src="images/icon2.jpg" alt=""></a></li>
                </ul>
              </article>
              <article class="col_3 pad_left2">
                <h3>Why Us:</h3>
                <ul class="list2">
                  <li><a href="#">Our Mission Statement </a></li>
                  <li><a href="#">Performance Report</a></li>
                  <li><a href="#">Prospective Parents </a></li>                  
                </ul>
              </article>
              <article class="col_4 pad_left2">
                <h3>Newsletter:</h3>
                <form id="newsletter" action="#" method="post">
                  <div class="wrapper">
                    <div class="bg">
                      <input type="text">
                    </div>
                  </div>
                  <a href="#" class="button"><span><span><strong>Subscribe</strong></span></span></a>
                </form>
              </article>
            </div>
            <div class="wrapper">
              <article class="call"> <span class="call1">Call Us Now: </span><span class="call2">041-722983</span> </article>              
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- / footer -->
  </div>
</div>
<script type="text/javascript">Cufon.now();</script>
</body>
</html>